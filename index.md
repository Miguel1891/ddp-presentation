---
theme: default
title: Data Driven Process
description: Please change this with a one-line description of your presentation
author: Shady, Ramez & Rodrigo
keywords: marp,marp-cli,slide
url: Please replace this with the URL of the deployed presentation
marp: true
image: Please replace this with a URL of an avatar/icon of your presentation
paginate: true
---

# Data Driven Process
## An efficient way to purchase and store materials
---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# The Problem of Site Layout Planning

Failure to prepare the site layout ahead of time is a major source of operational inefficiency and can significantly increase the overall cost of a project. The following issues may arise in the absence of a detailed site layout plan:

* Material stacks wrongly located. Materials arriving on site are off-loaded into what someone guesses to be the correct location. This problem may involve double or triple handling of materials to another location.
* Plant and equipment wrongly located.
* Inadequate space allowed. Where inadequate space is allowed for the stacking of materials or activities
* Site huts wrongly located in relation to their effective use

---
# The Problem of Building Material Supply Chain

Most of these problems are not generated in the conversion process but in the different interfaces that exist within the supply chain. Some of the general problems are as follows:
<!--* Lack of coordination, collaboration and commitment between suppliers and clients within the supply chain.-->
* Design problems (many changes and inconsistent information).
* Poor quality of materials and components.
* Deficient communication and information transfer.
* Inadequate management within the supply chain, mainly poor planning and control.
* Lack of effective methods for measuring the performance of the different parties.
<!--* Poor training of contractor’s suppliers, subcontractors and workers.-->
---
# Nowadays...

Whereas in construction works nowadays , field practitioners manually mark up a single site drawing to include major temporary facilities needed on site throughout the duration of the project. They depend on knowledge of years of experience, common sense, and adoption of past layouts in determining positions of temporary facilities on site. But, they can not keep track of all factors that could affect the selection, location, and interactions of all facilities to be positioned.

---
<!-- Start with the Mindmap from Miro
![bg 50%](.\assets\mindmap.png)

---
 -->
![bg 80%](assets/mindmap2.png)

---
![bg 70%](assets/mindmap1.png)

---
# General Idea

* It is a plugin
* Two connected processes
* The second process collect data from the first one
* The result could be visualized on a map
---
# How does it works?
## First Process
The first process collect data from Revit such as schedules and materials as well as suppliers databases.  
With the detailed information about the delivery dates, the plugin search in the web and/or the databases the "Best-Time-To-Purchase" a material and from which supplier.
This process delivers a new schedule with the new "Best-Possible-Dates".

---
# How does it works?
## Second Process
This part collects the information from the first process and compares it with the original schedule.  
After that a desicion is made to choose the final schedule with which we are going to work.  
With this schedule a map of the construction site will be generated to look where the materials could be sotred with the most efficiency.

---
# How it works - Sketch

---
<!--
@startuml
start

- Parameters Attached
  :Extract Materials Needed;
if (Material Prices) then (Not Fixed)
  :Scan For Suppliers\nTo Be Recommended;
 - Ask User to Select Best Recommendation
 - Output B
else (Fixed)
- Output A
endif

@enduml
-->

![bg 50%](assets/flowchart1.png)

---
@startuml
start
 if (Update Parameters to Create Output C) then 
  : Export Output C in a Form of Report ;
else 
 -Start Site Layout Simulation
- Divide Output C to Tasks 
- Set Location for each task in the Site Boundary Parameter
if ( Task Required at current Location ) then (yes)
 : Keep in set location ;
else (no)
 -Move to another location
 - Task needed in diffrent location later 
 -Create task timeline
endif
- Create DWG Layouts of all tasks loactions thru timeline 
- Export all DWG 
endif
- End Simulation

@enduml

---
# Timeline

## How are we going to work?
<!-- Instead of a checklist timeline, better a Gant diagramm @Rodrigo -->
<!-- - [x] Look for some experts opinions
- [0] Develop a first sketch for the whole project
- [] Look for already existant open-source codes
- [] Start the first-part code integration
- [] Start the second-part code integration
- [] Combine both parts

```mermaid
gantt
dateFormat  YYYY-MM-DD
title Timeline
excludes weekdays 2022

section Sketch
Look for experts opinion       :done,    des1, 2022-04-24,2022-05-09
Develop a first sketch for the whole project     :active,    des2, 2022-05-10, 2022-05-16
Look for already existant open-source codes    :active,    des3, 2022-05-17, 2022-05-23
section Integration
Start the first-part code         :active,    des4, 2022-05-24, 2022-06-13
Start the second-part code        :active,  des5, 2022-06-14, 2022-07-04
Combine both parts                :active, des6, 2022-07-05, 2022-07-12
```

@startuml

[Look for experts opinion] lasts 15 days
[Develop a first sketch] lasts 6 days
[Look for open-source codes] lasts 6 days
[Start the first-part code] lasts 20 days
[Start the second-part code] lasts 20 days
[Combine both parts] lasts 7 days


Project starts 2022-04-22
[Look for experts opinion] starts 2022-04-24
[Develop a first sketch] starts 2022-05-10
[Look for open-source codes] starts 2022-05-17
[Start the first-part code] starts 2022-05-24
[Start the second-part code] starts 2022-06-14
[Combine both parts] starts 2022-07-05

@enduml
-->
---

![bg 95%](assets/gantt_chart.JPG)

---
# Open Sources to be integrated
## Purchasing
* [purchase-workflow](https://github.com/OCA/purchase-workflow)
* [modern-data-warehouse-dataops](https://github.com/Azure-Samples/modern-data-warehouse-dataops)
## Warehouse
* [GreaterWMS](https://github.com/Singosgu/GreaterWMS)
* [openprocurement.api](https://github.com/openprocurement/openprocurement.api)

---
# Thank you for your attention!









<!--
# Regular slides are okay

> Quoted blocks are fancy, they add context

Paragraphs get to the point :) (check emoji textual representations [here](https://github.com/markdown-it/markdown-it-emoji/blob/master/lib/data/shortcuts.js))

---

# Mindmaps

But also, any **plantuml** diagram...

::: fence
@startuml

@startmindmap
* Big idea
** Smaller idea
*** Even smaller idea
*** ...
*** ...
** ...
** ...
@endmindmap

@enduml
:::
>